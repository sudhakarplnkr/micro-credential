﻿namespace CustomerApi.ViewModels
{
    public class CustomerViewModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}
